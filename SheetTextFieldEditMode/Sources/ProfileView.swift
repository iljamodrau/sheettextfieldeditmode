import SwiftUI

struct ProfileView: View {

	@State
	var name = ""

	@StateObject
	var viewModelStateObject = ProfileViewModel()

	@ObservedObject
	var viewModelObservedObject = ProfileViewModel()

	@Environment(\.editMode) var editMode

	var body: some View {
		VStack {

			Text("@State")
			TextField("TextField", text: $name)
				.disabled(editMode?.wrappedValue == .inactive)

			Text("@StateObject")
			TextField("TextField", text: $viewModelStateObject.name)
				.disabled(editMode?.wrappedValue == .inactive)

			Text("@ObservedObject")
			TextField("TextField", text: $viewModelObservedObject.name)
				.disabled(editMode?.wrappedValue == .inactive)

			Spacer()
		}
		.padding()
		.toolbar {
			ToolbarItem(placement: .navigationBarTrailing) {
				EditButton()
			}
		}
	}
}

struct ProfileView_Previews: PreviewProvider {
	static var previews: some View {
		ProfileView()
	}
}
