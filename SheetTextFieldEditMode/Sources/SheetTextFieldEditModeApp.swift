import SwiftUI

@main
struct SheetTextFieldEditModeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
