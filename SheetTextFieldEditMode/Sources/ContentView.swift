import SwiftUI

struct ContentView: View {

	@State
	var isPresentingEmtpyProfile: Bool = false

	var body: some View {

		NavigationView {

			List {
				NavigationLink(destination: ProfileView()) {
					Text("ProfileView")
				}

			}
			.toolbar {

				ToolbarItem(placement: .navigationBarTrailing) {
					Button(action: {
						isPresentingEmtpyProfile = true
					}) {
						Image(systemName: "plus")
					}
				}

			}
			.navigationBarTitle("Navigation", displayMode: .inline)
			.fullScreenCover(isPresented: $isPresentingEmtpyProfile) {
				NewProfileViewWithNavigation(isPresentingEmptyProfile: $isPresentingEmtpyProfile)
			}
		}
	}
}



extension ContentView {

	struct NewProfileViewWithNavigation: View {

		@State
		private var editMode = EditMode.active // new profile shows done button in the beginning


		@Binding
		var isPresentingEmptyProfile: Bool

		var body: some View {

			NavigationView {
				ProfileView()
					.navigationBarTitleDisplayMode(.inline)
					.toolbar {
						ToolbarItem(placement: .cancellationAction) {
							Button(action: {
								isPresentingEmptyProfile = false
							}) {
								Text("Close")
							}
						}
						ToolbarItem(placement: .principal) {
							Text("New Profile")
						}
					}
					// pass edit button status to edit button of ProfileView
					.environment(\.editMode, $editMode)
			}
		}
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}
